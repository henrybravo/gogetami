# gogetami
golang program to get the amazon ecs optimized ami id from public pages aws.amazon.com or github.com/awsdocs

## get binary


## or compile
To compile gogetami yourself, first download and install Go. On OSX:


`$ brew install go`


For other platforms check http://golang.org


### clone this repo
`$ git clone https://github.com/henrybravo/gogetami.git`


### build it
`$ go build gogetami.go`


## usage
Usage:
 `./gogetami <region>`
 
