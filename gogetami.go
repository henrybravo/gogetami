/*=========================================================
Name   : gogetami
Author : Henry Bravo
Date   : March 2018
About  : This is just a golang exercise program to get 
         amazon ecs ami id from the public aws pages 
         github.com/awsdocs or docs.aws.amazon.com for the 
         corresponding region argument that was taken from 
         cli. It requires the external library goquery: 
         github.com/PuerkitoBio/goquery
Usage  : gogetami region
Todo   : - Take source url from either argument or hardcode
         - Cache data if source is unavailable
         - Improve input argument checks
===========================================================*/

package main

import (
  "fmt"
  "log"
  "os"
  "regexp"
  "github.com/PuerkitoBio/goquery"
)

// declare globals
var searchregion string
var ami int
var validami = regexp.MustCompile("^ami-")
var debugging bool 
var helpargument bool

// program functions
func printUsage() {
  fmt.Println("usage: gogetami region\n")
}

func printHelp() {
  printUsage()
  fmt.Println("options: gogetami help or gogetami debug\n")
}

func printError(){
  fmt.Println("gogetami: error: please provide a valid aws region, e.g: us-east-[1-2], eu-west-[1-3]\n")
}

func getArguments(){
  // test arguments
  if (len(os.Args) < 2 || len(os.Args) > 2) {} else {
    // print debugging or set searchregion value
    switch os.Args[1] {
      case "help":
        printHelp()
        helpargument = true
      case "debug":
        fmt.Println("=> debug: this outputs everything found from source")
        debugging = true
      default:
        searchregion = os.Args[1]
    }
  }
}

func getAmi() {
  // declare slice to collect data
  records := make([]string, 0)

  // set data source
  //source := "https://github.com/awsdocs/amazon-ecs-developer-guide/blob/master/doc_source/ecs-optimized_AMI.md"
  source := "https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html"
  //source := "http://localhost:8080/ecs-optimized_AMI.md"
  
  // error handling
  doc, err := goquery.NewDocument(source)
    if err != nil {
      log.Fatal(err)
    }
 
  // find data from source
  doc.Find("td").Each(func (i int, s *goquery.Selection) {
    // put data into slice
    records = append(records, s.Text())
    // search ami while collecting data
    if s.Text() == searchregion {
      ami = i + 2
    }
    // debugging: output everything found
    if debugging {
      fmt.Println(i, s.Text())
    }
  })

  // output result if found
  if validami.MatchString(records[ami]) {
    fmt.Println(records[ami])
  } else if (debugging || helpargument) {} else {
    printHelp()
    printError()
  }
}

// start of program
func main() {
  getArguments()
  getAmi()
}
